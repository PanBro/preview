.PHONY: dev-up

dev-up:
	@ docker-compose up -d
	@ docker-compose run --rm php composer.phar install
