<?php


namespace tests;
use App\PreviewMaker;
use PHPUnit\Framework\TestCase;

class PreviewMakerTest extends TestCase
{
    /**
     * @param int $sizeOfPreview
     * @param string $rowSourceText
     * @param array $stopWords
     * @param string $expected
     * @param bool $setNeedCutFirstParagraph
     * @param bool $setNeedIncludeStopWord
     * @package test
     * @dataProvider makePreviewDataProvider
     */
    public function testMakePreview
    (
        int $sizeOfPreview,
        string $rowSourceText,
        array $stopWords,
        bool $setNeedCutFirstParagraph,
        bool $setNeedIncludeStopWord,
        string $expected
    )
    {
        $target = new PreviewMaker($sizeOfPreview);

        $target->setStopWord($stopWords);
        $target->setNeedCutFirstParagraph($setNeedCutFirstParagraph);
        $target->setNeedIncludeStopWord($setNeedIncludeStopWord);
        $actual = $target->makePreview($rowSourceText);

        static::assertSame($expected, $actual, 'функция makePreview работает не правильно');

    }

    public function makePreviewDataProvider()
    {
        return [
            'withSizeOnly' => [
                'sizeOfPreview' => 10,
                'rowSourceText' => 'Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое.',
                'stopWords' => [],
                'setNeedCutFirstParagraph' => true,
                'setNeedIncludeStopWord' => true,
                'expected' => 'Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что...'
            ],
            'withSizeOnlyAndWithTwoParagraph' => [
                'sizeOfPreview' => 10,
                'rowSourceText' => 'Проснувшись однажды утром после беспокойного 
                сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое.',
                'stopWords' => [],
                'setNeedCutFirstParagraph' => true,
                'setNeedIncludeStopWord' => true,
                'expected' => 'Проснувшись однажды утром после беспокойного...'
            ],
            'withSizeOnlyAndWithTwoParagraph_withUsingSecondParagraph' => [
                'sizeOfPreview' => 10,
                'rowSourceText' => 'Проснувшись однажды утром после беспокойного 
сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое.',
                'stopWords' => [],
                'setNeedCutFirstParagraph' => false,
                'setNeedIncludeStopWord' => true,
                'expected' => 'Проснувшись однажды утром после беспокойного 
сна, Грегор Замза обнаружил, что...'
            ],
            'withOneStopWord' => [
                'sizeOfPreview' => 10,
                'rowSourceText' => 'Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое.',
                'stopWords' => ['однажды'],
                'setNeedCutFirstParagraph' => true,
                'setNeedIncludeStopWord' => true,
                'expected' => 'Проснувшись однажды...'
            ],
            'withTwoStopWords' => [
                'sizeOfPreview' => 10,
                'rowSourceText' => 'Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое.',
                'stopWords' => [
                    'однажды',
                    'после'
                    ],
                'setNeedCutFirstParagraph' => true,
                'setNeedIncludeStopWord' => true,
                'expected' => 'Проснувшись однажды утром после...'
            ],
            'withTwoStopWords_withoutIncludingStopWords' => [
                'sizeOfPreview' => 10,
                'rowSourceText' => 'Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое.',
                'stopWords' => [
                    'однажды',
                    'после'
                    ],
                'setNeedCutFirstParagraph' => true,
                'setNeedIncludeStopWord' => false,
                'expected' => 'Проснувшись однажды утром...'
            ],
            'withTwoStopWords_withoutIncludingStopWords_withIncludingSecondParagraph' => [
                'sizeOfPreview' => 20,
                'rowSourceText' => 'Проснувшись однажды утром после беспокойного 
сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое.',
                'stopWords' => [
                    'обнаружил',
                    'сна'
                    ],
                'setNeedCutFirstParagraph' => false,
                'setNeedIncludeStopWord' => false,
                'expected' => 'Проснувшись однажды утром после беспокойного 
сна, Грегор Замза...'
            ],
            'withTwoStopWords_withIncludingMultitudesOfParagraphs' => [
                'sizeOfPreview' => 20,
                'rowSourceText' => 'Проснувшись 
однажды 
утром после беспокойного 
сна, Грегор Замза обнаружил, 
что он у себя в постели превратился в страшное насекомое.',
                'stopWords' => [
                    'обнаружил',
                    'сна'
                    ],
                'setNeedCutFirstParagraph' => false,
                'setNeedIncludeStopWord' => true,
                'expected' => 'Проснувшись 
однажды 
утром после беспокойного 
сна, Грегор Замза обнаружил...'
            ],
        ];
    }
}