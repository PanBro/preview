<?php


namespace App;


/**
 * Делает превью из исходного текста
 * @package App
 */
class PreviewMaker
{
    /** @var int количество слов, после которого нужно обрезать исходный текст */
    private $sizeOfPreview;
    /** @var string[] массив стоп-слов */
    private $stopWords = [];
    /** @var bool Надо ли включать стоп слово при обрезке */
    private $needIncludeStopWord = true;
    /** @var bool надо ли обрезать по первый параграф если на вход подано больше */
    private $needCutFirstParagraph = true;

    /** @var string результат */
    private $result;

    public function __construct(int $sizeOfPreview = 20)
    {
        $this->sizeOfPreview = $sizeOfPreview;
    }

    /** @param array $stopWords - записывает значение $stopWords */
    public function setStopWord(array $stopWords): void
    {
        $this->stopWords = $stopWords;
    }

    /** @return array - отдаёт значение $stopWords */
    public function getStopWords(): array
    {
        return $this->stopWords;
    }

    /** @return int - отдаёт значение $sizeOfPreview */
    public function getSizeOfPreview(): int
    {
        return $this->sizeOfPreview;
    }

    /** @param int $sizeOfPreview - записывает значение $sizeOfPreview */
    public function setSizeOfPreview(int $sizeOfPreview): void
    {
        $this->sizeOfPreview = $sizeOfPreview;
    }

    /** @return bool - отдаёт значение $needIncludeStopWord */
    public function isNeedIncludeStopWord(): bool
    {
        return $this->needIncludeStopWord;
    }

    /** @param bool $needIncludeStopWord - записывает значение $needIncludeStopWord */
    public function setNeedIncludeStopWord(bool $needIncludeStopWord): void
    {
        $this->needIncludeStopWord = $needIncludeStopWord;
    }

    /** @return bool - отдаёт значение $needCutFirstParagraph */
    public function isNeedCutFirstParagraph(): bool
    {
        return $this->needCutFirstParagraph;
    }

    /** @param bool $needCutFirstParagraph - записывает значение $needCutFirstParagraph */
    public function setNeedCutFirstParagraph(bool $needCutFirstParagraph): void
    {
        $this->needCutFirstParagraph = $needCutFirstParagraph;
    }


    /**
     * отдаёт строку с результатом отрезанного массива
     * @param $rowSourceText
     * @return string
     */
    public function makePreview(string $rowSourceText): string
    {
        $this->result = $rowSourceText;
        $this
            ->prepareSourceText()
            ->cutParagraph()
            ->cropText()
            ->cutWithStopWordInText()
            ->prepareToOutput();
        return $this->result;
    }

    /**
     * отрезает только первый абзац от текста, либо оставляет весь текст
     * @return PreviewMaker
     */
    private function cutParagraph(): PreviewMaker
    {
        if (!$this->needCutFirstParagraph) {
            return $this;
        }

        $secondParagraphStart = mb_strpos($this->result, PHP_EOL);
        if ($secondParagraphStart === false) {
            return $this;
        }

        if ($secondParagraphStart === 0) {
            $this->result = mb_substr($this->result, 1);
            return $this->cutParagraph();
        }

        $this->result = mb_substr($this->result, 0, $secondParagraphStart);
        return $this;
    }

    private function prepareSourceText(): PreviewMaker
    {
        $this->result = trim(preg_replace('~ {2,}~', ' ', $this->result));
        return $this;
    }

    /**
     * обрезает массив текста до указанного номера значения
     * @return PreviewMaker
     */
    private function cropText(): PreviewMaker
    {
        $words = explode(' ', $this->result);
        if (count($words) > $this->sizeOfPreview) {
            $words = array_slice($words, 0, $this->sizeOfPreview);
        }

        $this->result = implode(' ', $words);
        return $this;
    }

    /**
     * отрезает текст (уже обрезанный по длине) по последнему вхожденю стоп-слова
     * @return PreviewMaker
     */
    private function cutWithStopWordInText(): PreviewMaker
    {
        $words = explode(' ', $this->result);
        $reversedResultArray = array_reverse($words, true);

        $occurrence = null;
        $sourceTextWithoutSings = [];

        foreach ($this->stopWords as $stopWord) {

            foreach ($reversedResultArray as $sourceKey => $sourceValue) {
                $sourceTextHasOccurrence = mb_ereg('[0-9a-zA-Zа-яА-ЯёЁ\-]+', $sourceValue, $sourceTextWithoutSings);
                $processedSourceText = ($sourceTextHasOccurrence) ? $sourceTextWithoutSings[0] : null;
                if ($sourceKey >= $occurrence
                    &&
                    ($processedSourceText == $stopWord)
                ) {
                    $occurrence = $sourceKey;
                }
            }
        }
        if ($occurrence === null) {
            return $this;
        }
        if ($this->needIncludeStopWord) {
            $occurrence++;
        }
        $words = array_slice($words, 0, $occurrence);
        $this->result = implode(' ', $words);

        return $this;
    }

    /**
     * подготавливает текст к выводу: отрезает с конца знаки препинания, добавляет "..."
     * @return PreviewMaker
     */
    private function prepareToOutput(): PreviewMaker
    {
        $resultWithoutLastSings = [];
        mb_ereg('.+[^\,|\!|\?| |\.{1,3}]', $this->result, $resultWithoutLastSings);

        $this->result = $resultWithoutLastSings[0] . '...';
        return $this;
    }
}