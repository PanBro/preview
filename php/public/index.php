<?php declare(strict_types=1);


use App\PreviewMaker;

require_once 'vendor/autoload.php';
define('ROOT', dirname(__FILE__));

$previewMaker = new PreviewMaker(20);

$previewMaker->setNeedCutFirstParagraph(false);//по умолчанию - true
//$previewMaker->setNeedIncludeStopWord(true);//по умолчанию - true

$previewMaker->setStopWord([
    'обнаружил',
    'в',
]);
$textExample = $previewMaker->makePreview(
<<<'EOD'
Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, 
что он у себя в постели превратился в страшное насекомое.
EOD
);

var_dump($textExample);